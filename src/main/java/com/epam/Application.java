package com.epam;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <h1>Maven Project.</h1>
 * This application prints odd numbers from start to the end of interval
 * and even from end to start;
 * Application prints the sum of odd and even numbers;
 * Also application build Fibonacci numbers:
 * And find the biggest odd number
 * and the biggest even number in the Fibonacci numbers;
 * user can enter the size of set (N);
 * Application also prints percentage of odd and even Fibonacci numbers;
 *
 * @author Roman Pasichnyk
 * @version 1.0
 * @since 08.11.2019
 */

public class Application {

    /**
     * Main constructor
     */
    public Application() {
    }

    /**
     * The Application run from this method.
     * @param args for command-line arguments
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(new InputStreamReader(System.in));
        System.out.println("Enter start inverval");
        while (!sc.hasNextInt()) {
            System.out.println("Enter start inverval");
            sc.next();
        }
        int a = sc.nextInt();


        System.out.println("Enter end inverval");
        while (!sc.hasNextInt()) {
            System.out.println("Enter end inverval");
            sc.next();
        }
        int b = sc.nextInt();

        if (a > b) {
            int buf;
            buf = b;
            b = a;
            a = buf;
        }

        System.out.println("Odd numbers: " + checkOddNumbers(a, b));
        System.out.println("Even numbers: " + checkEvenNumbers(a, b));

        System.out.println();
        System.out.println("Enter size (>2) of Fibonacci numbers set");
        while (!sc.hasNextInt()) {
            System.out.println("Enter size (>2) of Fibonacci numbers set");
            sc.next();
        }
        int n;
        n = sc.nextInt();
        List<Integer> fibNumbers = buildFibNumbers(n);
        System.out.println("The Fibonacci numbers set: " + fibNumbers);
        System.out.println("The biggest odd number in the Fibonacci numbers set: "
                + biggestOddNumber(fibNumbers));
        System.out.println("The biggest even number in the Fibonacci numbers set: "
                + biggestEvenNumber(fibNumbers));

    }

    /**
     * The method find all odd numbers from interval [a,b].
     * @param a - the start of inverval;
     * @param b - the end of interval;
     * @return list of odd numbers;
     */
    private static List<Integer> checkOddNumbers(int a, int b) {
        List<Integer> oddNumbers = new ArrayList<Integer>();
        int oddSum = 0;
        for (int i = a; i <= b; i++) {
            if (i % 2 == 0) {
                oddNumbers.add(i);
                oddSum += i;
            }
        }
        System.out.println("Sum of odd numbers: " + oddSum);
        return oddNumbers;
    }

    /**
     * The method find all even numbers from interval [a,b].
     * @param a - the start of inverval;
     * @param b - the end of interval;
     * @return list of even numbers;
     */
    private static List<Integer> checkEvenNumbers(int a, int b) {
        List<Integer> evenNumbers = new ArrayList<Integer>();
        int evenSum = 0;
        for (int i = b; i >= a; i--) {
            if (i % 2 != 0) {
                evenNumbers.add(i);
                evenSum += i;
            }
        }

        System.out.println("Sum of even numbers: " + evenSum);
        return evenNumbers;
    }

    /**
     * The method build Fibonacci numbers.
     * @param n - size of Fibonacci set. User set this variable;
     * @return list of Fibonacci numbers;
     */
    private static List<Integer> buildFibNumbers(int n) {
        List<Integer> fibNumbers = new ArrayList<Integer>();
        int n0 = 0;
        int n1 = 1;
        n -= 2;
        fibNumbers.add(n0);
        fibNumbers.add(n1);
        int n2;
        for (int i = 1; i <= n; i++) {
            n2 = n0 + n1;
            n0 = n1;
            n1 = n2;
            fibNumbers.add(n2);
        }
        return fibNumbers;
    }

    /**
     * The method find the biggest odd number from Fibonacci numbers.
     * @param fibNumbers - List of Fibonacci numbers;
     * @return the biggest odd number from Fibonacci numbers;
     */
    private static int biggestOddNumber(List<Integer> fibNumbers) {
        int max = 0;
        int amountOddNumber = 0;
        for (int number : fibNumbers) {
            if (number % 2 == 0) {
                max = number;
                amountOddNumber++;
            }
        }
        float percentOddNumber = (float)amountOddNumber/fibNumbers.size() * 100;
        System.out.println("Percentage of odd Fibonacci numbers: " + percentOddNumber + "%");
        return max;
    }

    /**
     * The method find the biggest even number from Fibonacci numbers.
     * @param fibNumbers - List of Fibonacci numbers;
     * @return the biggest even number from Fibonacci numbers;
     */
    private static int biggestEvenNumber(List<Integer> fibNumbers) {
        int max = 0;
        int amountEvenNumber = 0;
        for (int number : fibNumbers) {
            if (number % 2 != 0) {
                max = number;
                amountEvenNumber++;
            }
        }
        float percentEvenNumber = (float)amountEvenNumber/fibNumbers.size() * 100;
        System.out.println("Percentage of even Fibonacci numbers: " + percentEvenNumber + "%");
        return max;
    }
}
